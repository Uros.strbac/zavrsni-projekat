<?php include("functions/init.php"); 

if(isset($_POST['checking'])) {
    $checked_services_ids = implode(',', $_POST['checking']);

        $sql = "SELECT service_id, service_duration FROM  services WHERE service_id IN ({$checked_services_ids})";
       
        $results =query($sql);
    
        
        $total_time = 0;
        foreach($results as $result){
            $service_durations = (int) $result['service_duration'];
            //var_dump ($service_durations);
            $total_time += $service_durations;
            $_SESSION["total_time"] = $total_time;

        }

       // die(json_encode(getReservedDates($total_time), JSON_PRETTY_PRINT));
        //echo json_encode(['2018-06-30', '2018-06-29'], JSON_PRETTY_PRINT);
        echo json_encode(getReservedDates($total_time), JSON_PRETTY_PRINT);
    }


/**
 * This dates need to be disabled in calendar
 */
function getReservedDates($choosen_services_duration) {
    $today = date('Y-m-d');

    $sql = "SELECT date, sum(duration) as duration FROM reservation WHERE date >= {$today} GROUP BY date ORDER BY date ASC";
    $results = query($sql);
    
    $dates = [];

    foreach($results as $result) {
        if ($result['duration'] + (int) $choosen_services_duration > (WORKING_DAY_HOURS_DURATION * 60))
            $date = explode('-', $result['date']);
            if (!empty($date)) {
                $dates[] = [
                    $date[0], 
                    $date[1] - 1, 
                    $date['2']
                ];
            }  
    }

    return $dates;
}

?>