<!-- <div>
		<?php display_message();?>
		<h1> Home </h1>
    </div> -->
    
    <!-- Carousel section-->
	 <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>

    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="img/piston.jpg">
        <div class="carousel-caption">
          <h1>automehanicar Dobrica Rasic</h1>

        </div>

      </div>
      <div class="item">
        <img src="img/motor.jpg">
      </div>

      <div class="item">
        <img src="img/spanner.jpg">
      </div>
    </div>
    <!---Slider Controls-->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span slass="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span slass="sr-only">Next</span>
    </a>
  </div>

    <div class="container text-center">
    <h2>What we're using</h2>
    <div class="row">
      <div class="col-sm-4">
        <img src="img/elf.png" id="icon">
      </div>


      <div class="col-sm-4">
        <img src="img/valvoline-logo.png" id="icon">
      </div>

      <div class="col-sm-4">
        <img src="img/total.jpg" id="icon">
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h4>PRODUCT RANGE</h4>
          <p>Quartz 9000, a synthetic oil providing superior levels of engine performance and protection. 
            It also provides TOTAL's highest levels of engine cleanliness. Quartz 9000 is long-lasting, 
            extending intervals between oil changes.<br>Quartz 7000, a synthetic-based oil that provides 
            enhanced driving comfort and high levels of engine cleanliness and protection.</p>
          <p>Quartz INEO, a synthetic oil with pollution control, specially formulated for use with diesel 
            engines. Quartz INEO provides exceptional engine cleanliness and supports particulate 
            filter performance.<br>The Quartz range also includes Quartz with ART (age resistance technology), 
            which improves your engine's ability to withstand mechanical ageing. Your engine's life will thus 
            be prolonged and maintenance costs will be reduced. Quartz with ART is formulated to be resistant 
            to both heat and cold, so it maintains a consistent viscosity across a range of temperatures.</p> 
        </div>
        <div class="col-md-6">
          <img src="img/ulje.jpg" class="img-responsive">
        </div>
      </div>
</div>

<!--Contact section-->  
<div id="contact" class="container">
      <div class="box">
        <div class="row">
          <div class="col-lg-12">
            <hr>
            <h2 class="intro-text text-center">Contact DOBRICA'S BUSINES</h2>
            <hr>
          </div>

          <div class="col-md-8">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2794.763727409978!2d19.866553115095236!3d45.53495997910191!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b381e449ef44f%3A0x67a99782f0806dc3!2sJovana+Popovic%CC%81a+79%2C+Turija!5e0!3m2!1sen!2srs!4v1532292830401" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>

          </div>
          <div class="col-md-4">
            <p>Phone:
              <strong>064/1343449</strong>
            </p>
            <p>Email:
                <strong><a href="mailto:carrepair1999@gmail.com">carrepair1999@gmail.com</a></strong>
              </p>
              <p>Adress:
                  <strong>21215 Turija<br>
                          Jovana Popovica 79 
                  </strong>
                </p>
          </div>

        </div>
      </div>
    

    <form action="mailto:carrepair1999@gmail.com">
      <div class="row">
        <div class="col-lg-12">
            <hr>
            <h2 class="intro-text text-center">Contact form</h2>
            <hr>
        </div>
        <div class="form-group col-lg-4">
          <label>Name</label>
          <input type="text" class="form-control">
        </div>
         
        <div class="form-group col-lg-4">
            <label>Email</label>
            <input type="email" class="form-control">
        </div>
        
        <div class="form-group col-lg-4">
            <label>Phone Number</label>
            <input type="tel" class="form-control">
        </div> 

        <div class="form-group col-lg-12">
            <label>Mesage</label>
            <textarea class="form-control" rows="6"></textarea>
        </div>

        <div class="form-group col-lg-12">
            <input type="hidden" name="save" value="contact">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>


      </div>
    </form> 
    </div>

      <!--Our team section--> 
    <div id="team" class="col-lg-12">
        <hr>
        <h2 class="intro-text text-center">Our team</h2>
        <hr>
    </div>
    <div id="team" class="team">
      <div class="container">
        <div class="row">
          <div  class="row">
            <p id="saban">Auto Centre has been in the 
              automotive industry for more than 50 years. Our 
              team offer superior automotive service and repair, 
              affordably priced with his customers' complete 
              satisfaction his top priority. We are pleased to 
              service both domestic and foreign vehicles, provide 
              emission inspections and repairs, brakes, transmissions, 
              suspension, oil changes, tune-ups, radiator repair, wheel 
              alignments and certifications.Our center is devoted to this 
              mission, and will continue to provide Automotive Service Excellence 
              with the highest level of Customer Satisfaction, while providing 
              a stress-free experience.</p>
          
          </div>
          <div class="column">
            <div class="imgBox">
              <img src="img/dobrica.jpg">
            </div>
            <div class="details">
              <h3>Dobrica Rasic<br><span>CEO</span></h3>
            </div>
          </div>
          <div class="column">
              <div class="imgBox">
                  <img src="img/mika.jpg">
                </div>
                <div class="details">
                  <h3>Mika<br><span>Mechanics</span></h3>
                </div>
          </div>
          <div class="column">
              <div class="imgBox">
                  <img src="img/pera.jpg">
                </div>
                <div class="details">
                  <h3>Pera<br><span>Mechanics</span></h3>
                </div>
          </div>
          <div class="column">
              <div class="imgBox">
                  <img src="img/zika.jpg">
                </div>
                <div class="details">
                  <h3>Zika<br><span>Mechanics</span></h3>
                </div>
          </div>
        </div>  
      </div>
    </div>


<div id="prices" class="col-lg-12">
        <hr>
        <h2 class="intro-text text-center">Price list</h2>
        <hr>
</div>

<?php 
$sql = "SELECT * FROM services";
$results =query($sql);
// var_dump($results);

foreach ($results as $result) {
    $service_name = $result['service_name'];
    $service_price = $result['service_price'];
    
    $service_description = $result['service_description'];
    
    
    echo "<div id='price_list'>";
    echo "Name: ".$service_name;
    
    echo "<br>";
    echo "Description: ".$service_description;
    echo "<br>";
   
   
    echo "Price: " .$service_price." USD";
    
    
    echo "</div>";
}

?>
