<?php  include 'includes/header.php' ?>
<?php
if(logged_in()) {
	redirect("index.php");
}

?>



<?php include 'includes/nav.php' ?>



	<div class="row">
		<?php display_message(); ?>
		<?php validate_user_login(); ?>
	</div>
    
		<a href="login.php" class="active" id="login-form-link">Login</a>
		<a href="register.php" id="">Register</a>
							
					
		<form id="login-form"  method="post" role="form" style="display: block;">
			<input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="Email" required>
			<input type="password" name="password" id="login-password" tabindex="2" class="form-control" placeholder="Password" required>
			<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
			<label for="remember"> Remember Me</label>

			<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
									
			<a href="recover.php" tabindex="5" class="forgot-password">Forgot Password?</a>
												

<?php include 'includes/footer.php' ?>
